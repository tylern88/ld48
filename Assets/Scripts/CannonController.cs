using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    public GameObject missile;

    public void FireMissile(Vector2 velocity, Transform parentTransform)
    {
        var position = parentTransform.position;
        Instantiate(missile, new Vector3(position.x, position.y), parentTransform.rotation);
    }

    private void Update()
    {
        
    }
}
