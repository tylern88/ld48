using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

public class ScrollScript : MonoBehaviour
{
  float scrollspeed = 5f;
  Vector2 startPos;

  private float altitude = 0.0f;

  [SerializeField]
  private bool isScrolling = false;

  private TextMeshProUGUI distance;

  // Start is called before the first frame update
  void Start()
  {
    startPos = transform.position;
    distance = GameObject.Find("Distance Value").GetComponent<TextMeshProUGUI>();
  }

  // Update is called once per frame
  void Update()
  {

    if (isScrolling)
    {
      float newPos = Mathf.Repeat(Time.time * scrollspeed, 65);
      transform.position = startPos + Vector2.down * newPos;
    }
  }

  private void FixedUpdate()
  {
    if (isScrolling)
    {
      altitude += Math.Abs(((startPos + Vector2.down * Mathf.Repeat(Time.time * scrollspeed, 65)).y) * Time.deltaTime);
    }
    
    distance.SetText(Math.Ceiling(altitude).ToString(CultureInfo.CurrentCulture));
  }

  public void StartBackgroundScroll()
  {
    isScrolling = true;
  }



}
