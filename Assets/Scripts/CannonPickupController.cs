using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonPickupController : MonoBehaviour
{
    public float rotationSpeed = 25;
    private void OnTriggerEnter2D(Collider2D other)
    {
        var controller = other.GetComponent<RocketController>();

        if (!controller) {return;}
        
        controller.AttachCannon();
        Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        transform.Rotate(0, 0, -(Time.deltaTime * rotationSpeed));
    }
}
