using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceFieldPickupController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        var controller = other.GetComponent<RocketController>();

        if (!controller) {return;}
        
        controller.StartForceField();
        Destroy(gameObject);
    }
}
