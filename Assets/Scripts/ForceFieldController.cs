using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceFieldController : MonoBehaviour
{
    public float activeTime = 3f;
    
    private void Start()
    {
        Destroy(gameObject, activeTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("force field controller other: " + other);
        var controller = other.GetComponent<DestroyableController>();

        if (controller)
        {
            Destroy(controller.gameObject);
        }
    }
}