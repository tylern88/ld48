using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceMissileController : MonoBehaviour
{
    [SerializeField]
    private float projectileSpeed = 20.0f;
    [SerializeField]
    private GameObject explosion;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("space missile controller other: " + other);

        var controller = other.GetComponent<DestroyableController>();
        
        if (!controller) {return;}

        Instantiate(explosion, controller.transform.position, Quaternion.identity);
        Destroy(controller.gameObject);
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.up * (projectileSpeed * Time.deltaTime));
    }
}