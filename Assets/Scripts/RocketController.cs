using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    [SerializeField]
    private float speed = 300.0f;
    [SerializeField]
    private float fuel = 100.0f;
    [SerializeField]
    private float maxFuel = 100;
    [SerializeField]
    private float fuelConsumption = 10.0f;
    [SerializeField]
    private GameObject forceFieldPowerUp;
    [SerializeField]
    private GameObject cannonPowerUp;
    [SerializeField]
    private bool canMove = false;

    private Rigidbody2D rocketRigidBody2d;
    private float horizontal = 0f;
    private float vertical = 0f;
    private bool cannonActive = false;
    private CannonController cannonController;
    private TextMeshProUGUI fuelValue;

    public void StartForceField()
    {
        Instantiate(forceFieldPowerUp, transform);
    }

    public void AttachCannon()
    {
        if (cannonActive)
        {
            return;
        }

        cannonActive = true;
        Instantiate(cannonPowerUp, transform);
        cannonController = cannonPowerUp.GetComponent<CannonController>();
    }

    public void AddFuel(float fuelTank)
    {
        fuel = Mathf.Clamp((fuel + fuelTank), 0, maxFuel);
    }

    public void SetCanMove(bool toggle)
    {
        canMove = toggle;
    }

    // Start is called before the first frame update
    private void Start()
    {
        rocketRigidBody2d = GetComponent<Rigidbody2D>();
        fuelValue = GameObject.Find("Fuel Value").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (canMove)
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");   
        }

        if (!Input.GetKeyUp(KeyCode.Space) || !cannonActive) {return;}
        
        var velocity = rocketRigidBody2d.velocity;
        cannonController.FireMissile(velocity, transform);
    }

    private void FixedUpdate()
    {
        var velocity = rocketRigidBody2d.velocity;
        var xVel = velocity.x;
        var yVel = velocity.y;

        if (fuel < 0)
        {
            return;
        }

        if (vertical != 0)
        {
            fuel -= fuelConsumption * Math.Abs(vertical) * Time.deltaTime;
            yVel = speed * vertical * Time.deltaTime;
        }

        if (horizontal != 0)
        {
            fuel -= fuelConsumption * Math.Abs(horizontal) * Time.deltaTime;
            xVel = speed * horizontal * Time.deltaTime;
        }
        
        fuelValue.SetText((Math.Ceiling(fuel).ToString(CultureInfo.CurrentCulture)));

        var newVelocity = new Vector2(xVel, yVel);
        var magnitude = newVelocity.magnitude;

        var angle = Mathf.Atan2(newVelocity.y, newVelocity.x) * Mathf.Rad2Deg - 90;

        if ((magnitude < 1 && magnitude > -1) || horizontal == 0)
        {
            angle = 0;
        }

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        rocketRigidBody2d.velocity = newVelocity;
    }
}