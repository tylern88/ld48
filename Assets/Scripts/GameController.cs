using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
  public static GameController instance;
  public GameObject CloudPrefab, AsteroidPrefab, PlanetPrefab, FuelPrefab;

  public GameObject spaceBackground;

  [SerializeField]
  private bool isGameRunning = false;

  [SerializeField]
  private float altitude = 0.0f;

  private GameObject rocket;
  private RocketController rocketController;

  private ScrollScript scrollScript;

  private TextMeshProUGUI launchText;

  private void Start()
  {
    rocket = GameObject.Find("Rocket");
    rocketController = rocket.GetComponent<RocketController>();
    scrollScript = spaceBackground.GetComponent<ScrollScript>();
    launchText = GameObject.Find("Launch Text").GetComponent<TextMeshProUGUI>();
  }

  private void Update()
  {
    if (Input.GetKeyUp(KeyCode.Space))
    {
      StartGame();
      rocketController.SetCanMove(true);
      launchText.SetText("");
    }
  }
  public void StartGame()
  {
    scrollScript.StartBackgroundScroll();
    isGameRunning = true;
  }

  public void ResetGame()
  {
    isGameRunning = true;
  }

  private void FixedUpdate()
  {
    // spawn all the things at a given time
    if (isGameRunning)
    {
      SpawnFuel();

      if (altitude < 1000)
      {
        // SpawnClouds();
      }

      // need to determin a good altitude
      if (altitude >= 1000)
      {
        SpawnAsteroids();
        SpawnPlanets();
      }

    }
  }

  public bool GetIsGameRunning()
  {
    return isGameRunning;
  }
  public void GameOver()
  {
    isGameRunning = false;
  }

  // Spawns
  // We should determin a good interval for when these spawn.
  private void SpawnClouds()
  {
    Vector3 pos = GetRocketPosition();
    Instantiate(CloudPrefab, pos, Quaternion.identity);
  }

  private void SpawnPlanets()
  {
    Vector3 pos = GetRocketPosition();
    Instantiate(PlanetPrefab, pos, Quaternion.identity);
  }

  private void SpawnAsteroids()
  {
    Vector3 pos = GetRocketPosition();
    Instantiate(AsteroidPrefab, pos, Quaternion.identity);
  }

  private void SpawnFuel()
  {
    // we need to calculate the position of the spawns based on where the rocket is at the given time
    Vector3 pos = GetRocketPosition();
    // Instantiate(FuelPrefab, pos, Quaternion.identity);
  }

  private Vector3 GetRocketPosition()
  {
    return rocket.transform.position;
  }


  // destroy prefabs once they are out of view





}
