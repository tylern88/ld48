using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceStation : MonoBehaviour
{
  private bool isGameRunning;

  private Vector2 startPos;
  private float movementSpeed = 5;
  // Start is called before the first frame update

  void Start()
  {
    startPos = transform.position;
  }

  // Update is called once per frame
  void Update()
  {


  }

  private void FixedUpdate()
  {
    GetIsGameRunning();
    if (isGameRunning)
    {
      transform.Translate(0, -movementSpeed * Time.deltaTime, 0);

      if (transform.position.y < -20)
      {
        Destroy(gameObject);
      }
    }
  }

  void GetIsGameRunning()
  {

    GameController gameController = GameObject.FindObjectOfType<GameController>();
    isGameRunning = gameController.GetIsGameRunning();

  }
}
